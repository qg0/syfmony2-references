<?php

namespace KreaLab\CommonBundle\Assetic\Filter;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;

class Cutcomments implements FilterInterface
{
    /**
     * @inheritdoc
     */
    public function filterLoad(AssetInterface $asset)
    {
    }

    public function filterDump(AssetInterface $asset)
    {
        $content = $asset->getContent();
        $content = preg_replace('#/\*.*?\*/#s', '', $content);
        $asset->setContent($content);
    }
}
