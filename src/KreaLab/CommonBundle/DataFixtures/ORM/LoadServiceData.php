<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\CommonBundle\Entity\Discount;
use KreaLab\CommonBundle\Entity\Service;
use KreaLab\CommonBundle\Entity\ServiceDiscount;
use KreaLab\CommonBundle\Entity\ReferenceType;

class LoadServiceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $referenceType = new ReferenceType();
        $referenceType->setName('Тип бланка 1 водительская');
        $referenceType->setDriverReference(true);
        $referenceType->setIsSerie(1);
        $manager->persist($referenceType);

        $service = new Service();
        $service->setName('Услуга №1 ВС');
        $service->setDesc('-');
        $service->setPrice(2100);
        $service->setPosition(0);
        $service->setCode(1);
        $service->setIsNotRevisitPrice(false);
        $service->setReferenceType($referenceType);
        $manager->persist($service);

        $discount = new Discount();
        $discount->setIsOnline(true);
        $discount->setName('Скидка1');
        $manager->persist($discount);

        $serviceDiscount = new ServiceDiscount();
        $serviceDiscount->setActive(true);
        $serviceDiscount->setSum(100);
        $serviceDiscount->setService($service);
        $serviceDiscount->setDiscount($discount);
        $manager->persist($serviceDiscount);

        $referenceType = new ReferenceType();
        $referenceType->setName('Тип бланка 2 водительская');
        $referenceType->setDriverReference(true);
        $referenceType->setIsSerie(1);

        $manager->persist($referenceType);

        $service = new Service();
        $service->setName('Услуга №2 ВС ГНОЧ');
        $service->setDesc('-');
        $service->setPrice(3200);
        $service->setMinPrice(500);
        $service->setPosition(1);
        $service->setCode(2);
        $service->setIsGnoch(true);
        $service->setIsNotRevisitPrice(false);
        $service->setReferenceType($referenceType);
        $manager->persist($service);

        $discount = new Discount();
        $discount->setIsOnline(false);
        $discount->setName('Скидка2');
        $manager->persist($discount);

        $serviceDiscount = new ServiceDiscount();
        $serviceDiscount->setActive(true);
        $serviceDiscount->setSum(200);
        $serviceDiscount->setService($service);
        $serviceDiscount->setDiscount($discount);
        $manager->persist($serviceDiscount);

        $referenceType = new ReferenceType();
        $referenceType->setName('Тип бланка 3 водительская');
        $referenceType->setDriverReference(true);
        $referenceType->setIsSerie(1);
        $manager->persist($referenceType);

        $service = new Service();
        $service->setName('Услуга №3 ВС ЭЭГ ГНОЧ');
        $service->setDesc('-');
        $service->setPrice(5300);
        $service->setPosition(2);
        $service->setCode(3);
        $service->setIsGnoch(true);
        $service->setIsEegConclusion(true);
        $service->setIsNotRevisitPrice(false);
        $service->setReferenceType($referenceType);
        $manager->persist($service);

        $discount = new Discount();
        $discount->setIsOnline(false);
        $discount->setName('Скидка3');
        $manager->persist($discount);

        $serviceDiscount = new ServiceDiscount();
        $serviceDiscount->setActive(true);
        $serviceDiscount->setSum(300);
        $serviceDiscount->setService($service);
        $serviceDiscount->setDiscount($discount);
        $manager->persist($serviceDiscount);

        $referenceType = new ReferenceType();
        $referenceType->setName('Тип бланка 4');
        $referenceType->setDriverReference(false);
        $referenceType->setIsSerie(1);
        $manager->persist($referenceType);

        $service = new Service();
        $service->setName('Услуга №4');
        $service->setDesc('-');
        $service->setPrice(320);
        $service->setMinPrice(50);
        $service->setPosition(3);
        $service->setCode(4);
        $service->setIsNotRevisitPrice(false);
        $service->setReferenceType($referenceType);
        $manager->persist($service);

        $discount = new Discount();
        $discount->setIsOnline(false);
        $discount->setName('Скидка4');
        $manager->persist($discount);

        $serviceDiscount = new ServiceDiscount();
        $serviceDiscount->setActive(true);
        $serviceDiscount->setSum(400);
        $serviceDiscount->setService($service);
        $serviceDiscount->setDiscount($discount);
        $manager->persist($serviceDiscount);

        $referenceType = new ReferenceType();
        $referenceType->setName('Тип бланка 5');
        $referenceType->setDriverReference(false);
        $referenceType->setIsSerie(1);
        $manager->persist($referenceType);

        $service = new Service();
        $service->setName('Услуга №5');
        $service->setDesc('-');
        $service->setPrice(4000);
        $service->setMinPrice(540);
        $service->setPosition(4);
        $service->setCode(5);
        $service->setIsNotRevisitPrice(false);
        $service->setReferenceType($referenceType);
        $manager->persist($service);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
