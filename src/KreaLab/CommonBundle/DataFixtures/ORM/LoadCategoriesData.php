<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\CommonBundle\Entity\Category;

class LoadCategoriesData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category = new Category();
        $category->setName('A');
        $manager->persist($category);

        $category = new Category();
        $category->setName('B');
        $manager->persist($category);

        $category = new Category();
        $category->setName('C');
        $manager->persist($category);

        $category = new Category();
        $category->setName('D');
        $manager->persist($category);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
