<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\CommonBundle\Entity\LegalEntity;

class LoadLegalEntityData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $legalEntity = new LegalEntity();
        $legalEntity->setName('Юридическое лицо №1');
        $legalEntity->setInn('1');
        $legalEntity->setAddress('-');
        $legalEntity->setPerson('-');
        $legalEntity->setLicense('159');
        $legalEntity->setCheckingAccount('1');
        $legalEntity->setBankName('Bank 1');
        $legalEntity->setCorrespondentAccount('123567846');
        $legalEntity->setKpp('6998');
        $legalEntity->setBik('1123313');
        $legalEntity->setPersonGenitive('ggg');
        $legalEntity->setPhone('777777');
        $legalEntity->setShortName(1);
//        $legalEntity->setRequisites('-');
        $manager->persist($legalEntity);

        $legalEntity = new LegalEntity();
        $legalEntity->setName('Юридическое лицо №2');
        $legalEntity->setInn('2');
        $legalEntity->setAddress('-');
        $legalEntity->setPerson('-');
        $legalEntity->setLicense('159');
        $legalEntity->setCheckingAccount('1');
        $legalEntity->setBankName('Bank 2');
        $legalEntity->setCorrespondentAccount('123567846');
        $legalEntity->setKpp('6998');
        $legalEntity->setBik('1123313');
        $legalEntity->setPersonGenitive('ggg');
        $legalEntity->setPhone('777777');
        $legalEntity->setShortName(2);
//        $legalEntity->setRequisites('-');
        $manager->persist($legalEntity);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
