<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\CommonBundle\Entity\Filial;

class LoadFilialData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $filial = new Filial();
        $filial->setName('Филиал №1');
        $filial->setNameShort('Ф1');
        $filial->setAddress('-');
        $filial->setIps(['127.0.0.1']);
        $manager->persist($filial);

        $filial = new Filial();
        $filial->setName('Филиал №2');
        $filial->setNameShort('Ф2');
        $filial->setAddress('-');
        $filial->setIps(['127.0.0.1']);
        $manager->persist($filial);

        $filial = new Filial();
        $filial->setName('Филиал №3');
        $filial->setNameShort('Ф3');
        $filial->setAddress('-');
        $filial->setIps(['127.0.0.1']);
        $manager->persist($filial);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
