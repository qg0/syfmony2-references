<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\CommonBundle\Entity\Discount;
use KreaLab\CommonBundle\Entity\Envelope;
use KreaLab\CommonBundle\Entity\LegalEntity;
use KreaLab\CommonBundle\Entity\Service;
use KreaLab\CommonBundle\Entity\ServiceDiscount;
use KreaLab\CommonBundle\Entity\ServiceLog;
use KreaLab\CommonBundle\Entity\Filial;
use KreaLab\CommonBundle\Entity\Workplace;
use KreaLab\CommonBundle\Entity\User;
use KreaLab\CommonBundle\Entity\Cashbox;

class LoadServiceLog extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $service   = $manager->getRepository('CommonBundle:Service')->findOneBy([]);
        $workplace = $manager->getRepository('CommonBundle:Workplace')->findOneBy([]);
        $cashbox   = $manager->getRepository('CommonBundle:Cashbox')->findOneBy([]);
        $user      = $manager->getRepository('CommonBundle:User')->findOneBy([]);

        $discount = new Discount();
        $discount->setName('Скидка №1');
        $discount->setIsOnline(false);
        $manager->persist($discount);

        $serviceDiscount = new ServiceDiscount();
        $serviceDiscount->setActive(true);
        $serviceDiscount->setSum(100);
        $serviceDiscount->setService($service);
        $serviceDiscount->setDiscount($discount);
        $manager->persist($serviceDiscount);

        $envelope = new Envelope();
        $envelope->setWorkplace($workplace);
        $envelope->setOperator($user);
        $envelope->setCourier($user);
        $envelope->setSum(100);
        $envelope->setCourierDatetime(new \DateTime('now'));
        $manager->persist($envelope);

        $serviceLog = new ServiceLog();
        $serviceLog->setService($service);
        $serviceLog->setWorkplace($workplace);
        $serviceLog->setOperator($user);
        $serviceLog->setCashbox($cashbox);
        $serviceLog->setEnvelope($envelope);
        $serviceLog->setSum(1000);
        $serviceLog->setDateGiving(new \DateTime('now'));
        $serviceLog->setFirstName('Пётр');
        $serviceLog->setLastName('Петров');
        $serviceLog->setPatronymic('Петрович');
        $serviceLog->setBirthday(new \DateTime('now'));
        $serviceLog->setNum('9-1-1-15763763');
        $serviceLog->setNumBlank('12345');
        $manager->persist($serviceLog);
        $manager->flush();
    }

    public function getOrder()
    {
        return 100;
    }
}
