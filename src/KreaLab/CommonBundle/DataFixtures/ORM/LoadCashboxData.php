<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\CommonBundle\Entity\Cashbox;

class LoadCashboxData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $workplace = $manager->getRepository('CommonBundle:Workplace')->findOneBy([
            'name' => 'Рабочее место №4',
        ]);

        $legalEntity = $manager->getRepository('CommonBundle:LegalEntity')->findOneBy([
            'name' => 'Юридическое лицо №1',
        ]);

        $cashbox = new Cashbox();
        $cashbox->setNum('№1');
        $cashbox->setInvNum('0001');
        $cashbox->setLegalEntity($legalEntity);
        $cashbox->setWorkplace($workplace);
        $manager->persist($cashbox);

        $cashbox = new Cashbox();
        $cashbox->setNum('№2');
        $cashbox->setInvNum('0002');
        $cashbox->setLegalEntity($legalEntity);
        $cashbox->setWorkplace($workplace);
        $manager->persist($cashbox);

        $legalEntity = $manager->getRepository('CommonBundle:LegalEntity')->findOneBy([
            'name' => 'Юридическое лицо №2',
        ]);

        $cashbox = new Cashbox();
        $cashbox->setNum('№3');
        $cashbox->setInvNum('0003');
        $cashbox->setLegalEntity($legalEntity);
        $cashbox->setWorkplace($workplace);
        $manager->persist($cashbox);

        $cashbox = new Cashbox();
        $cashbox->setNum('№4');
        $cashbox->setInvNum('0004');
        $cashbox->setLegalEntity($legalEntity);
        $cashbox->setWorkplace($workplace);
        $manager->persist($cashbox);

        $cashbox = new Cashbox();
        $cashbox->setNum('№5');
        $cashbox->setInvNum('0005');
        $cashbox->setLegalEntity($legalEntity);
        $manager->persist($cashbox);

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
