<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\CommonBundle\Entity\Filial;
use KreaLab\CommonBundle\Entity\Workplace;

class LoadWorkplaceData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $filial      = $manager->getRepository('CommonBundle:Filial')->findOneBy(['name_short' => 'Ф1']);
        $legalEntity = $manager->getRepository('CommonBundle:LegalEntity')->findOneBy(['inn' => '1']);

        $workplace = new Workplace();
        $workplace->setName('Рабочее место №1');
        $workplace->setFilial($filial);
        $workplace->setLegalEntity($legalEntity);
        $manager->persist($workplace);

        $workplace = new Workplace();
        $workplace->setName('Рабочее место №2');
        $workplace->setFilial($filial);
        $workplace->setLegalEntity($legalEntity);
        $manager->persist($workplace);

        $filial = $manager->getRepository('CommonBundle:Filial')->findOneBy(['name_short' => 'Ф2']);

        $workplace = new Workplace();
        $workplace->setName('Рабочее место №3');
        $workplace->setFilial($filial);
        $workplace->setLegalEntity($legalEntity);
        $manager->persist($workplace);

        $legalEntity = $manager->getRepository('CommonBundle:LegalEntity')->findOneBy(['inn' => 2]);

        $workplace = new Workplace();
        $workplace->setName('Рабочее место №4');
        $workplace->setFilial($filial);
        $workplace->setLegalEntity($legalEntity);
        $manager->persist($workplace);

        $filial = $manager->getRepository('CommonBundle:Filial')->findOneBy(['name_short' => 'Ф3']);

        $workplace = new Workplace();
        $workplace->setName('Рабочее место №5');
        $workplace->setFilial($filial);
        $workplace->setLegalEntity($legalEntity);
        $manager->persist($workplace);

        $workplace = new Workplace();
        $workplace->setName('Рабочее место №6');
        $workplace->setFilial($filial);
        $workplace->setLegalEntity($legalEntity);
        $manager->persist($workplace);

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
