<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\CommonBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $encoder = $this->container->get('security.password_encoder');

        $user = new User();
        $user->setUsername('admin');
        $user->setRoles(['ROLE_SUPERADMIN']);
        $user->setPassword($encoder->encodePassword($user, 'admin'));
        $user->setLastName('admin');
        $user->setFirstName('admin');
        $user->setPatronymic('super_admin');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(0);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('courier');
        $user->setRoles(['ROLE_COURIER']);
        $user->setPassword($encoder->encodePassword($user, 'courier'));
        $user->setLastName('courier');
        $user->setFirstName('courier');
        $user->setPatronymic('courier');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(0);
        $filials = $manager->getRepository('CommonBundle:Filial')->findAll();
        foreach ($filials as $filial) {
            $user->addFilial($filial);
        }

        $manager->persist($user);

        $workplace = $manager->getRepository('CommonBundle:Workplace')->findOneBy(['name' => 'Рабочее место №4']);

        $user = new User();
        $user->setUsername('operator');
        $user->setRoles(['ROLE_OPERATOR']);
        $user->setPassword($encoder->encodePassword($user, 'operator'));
        $user->setLastName('operator');
        $user->setFirstName('operator');
        $user->setPatronymic('operator');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(0);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('senior_operator');
        $user->setRoles(['ROLE_SENIOR_OPERATOR']);
        $user->setPassword($encoder->encodePassword($user, 'senior_operator'));
        $user->setFirstName('senior_operator');
        $user->setLastName('senior_operator');
        $user->setPatronymic('senior_operator');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(0);
        $user->setForceChangePass(0);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('treasurer');
        $user->setRoles(['ROLE_TREASURER']);
        $user->setPassword($encoder->encodePassword($user, 'treasurer'));
        $user->setLastName('treasurer');
        $user->setFirstName('treasurer');
        $user->setPatronymic('treasurer');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(0);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('orderman');
        $user->setRoles(['ROLE_ORDERMAN']);
        $user->setPassword($encoder->encodePassword($user, 'orderman'));
        $user->setLastName('orderman');
        $user->setFirstName('orderman');
        $user->setPatronymic('orderman');
        $user->setOrdermanSum(100000);
        $user->setSupervisorSum(0);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('acquittanceman');
        $user->setRoles(['ROLE_ACQUITTANCEMAN']);
        $user->setPassword($encoder->encodePassword($user, 'acquittanceman'));
        $user->setLastName('acquittanceman');
        $user->setFirstName('acquittanceman');
        $user->setPatronymic('acquittanceman');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(0);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('supervisor');
        $user->setRoles(['ROLE_SUPERVISOR']);
        $user->setPassword($encoder->encodePassword($user, 'supervisor'));
        $user->setLastName('supervisor');
        $user->setFirstName('supervisor');
        $user->setPatronymic('supervisor');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(1000000);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('stockman');
        $user->setRoles(['ROLE_STOCKMAN']);
        $user->setPassword($encoder->encodePassword($user, 'stockman'));
        $user->setLastName('stockman');
        $user->setFirstName('stockman');
        $user->setPatronymic('stockman');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(1000000);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('referenceman');
        $user->setRoles(['ROLE_REFERENCEMAN']);
        $user->setPassword($encoder->encodePassword($user, 'referenceman'));
        $user->setLastName('referenceman');
        $user->setFirstName('referenceman');
        $user->setPatronymic('referenceman');
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(1000000);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('archivarius');
        $user->setRoles(['ROLE_ARCHIVARIUS']);
        $user->setPassword($encoder->encodePassword($user, 'archivarius'));
        $user->setLastName('archivarius');
        $user->setFirstName('archivarius');
        $user->setPatronymic('archivarius');
        $user->setForceChangePass(0);
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(1200);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $user = new User();
        $user->setUsername('cashier');
        $user->setRoles(['ROLE_CASHIER']);
        $user->setPassword($encoder->encodePassword($user, 'cashier'));
        $user->setLastName('cashier');
        $user->setFirstName('cashier');
        $user->setPatronymic('cashier');
        $user->setForceChangePass(0);
        $user->setOrdermanSum(0);
        $user->setSupervisorSum(1200);
        $user->setWorkplace($workplace);
        $manager->persist($user);

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
