<?php

namespace KreaLab\CommonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KreaLab\AdminSkeletonBundle\Entity\Setting;

class LoadSettingsData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $setting = new Setting();
        $setting->setKey('orderman_close_order_sum');
        $setting->setValue(1000);
        $setting->setType('integer');
        $manager->persist($setting);

        $setting = new Setting();
        $setting->setKey('orderman_overrun');
        $setting->setValue(0.1);
        $setting->setType('float');
        $manager->persist($setting);

        $setting = new Setting();
        $setting->setKey('operator_blanks_on_hands_lost_title');
        $setting->setValue('[Текст] Предупреждение на странице подтверждения утери бланка: {Заголовок}');
        $manager->persist($setting);

        $setting = new Setting();
        $setting->setKey('operator_blanks_on_hands_lost_text');
        $setting->setValue('[Текст] Предупреждение на странице подтверждения утери бланка: {Текст}');
        $manager->persist($setting);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
