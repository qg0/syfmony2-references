<?php

namespace KreaLab\AppBundle\Controller\ReferenceMan;

use Doctrine\ORM\EntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ReportingForReferenceController extends Controller
{
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;

    public function init()
    {
        $this->em = $this->get('doctrine.orm.entity_manager');
        $this->denyAccessUnlessGranted('ROLE_REFERENCEMAN');
    }

    /**
     * @Route("/report-blanks/", name="rm_report_blanks")
     */
    public function listAction(Request $request)
    {
        $usedStatus = ['acceptedByOperator', 'lostByOperator', 'cancelledByOperator', 'archivedByReferenceman', 'lost'];

        $qb = $this->em->getRepository('CommonBundle:User')->createQueryBuilder('u')
            ->andWhere('u.roles LIKE :role')->setParameter('role', '%ROLE_OPERATOR%')
            ->leftJoin('u.operator_blanks', 'bl')->addSelect('bl')
            ->andWhere('bl.status IN (:status)')->setParameter('status', $usedStatus)
            ->addOrderBy('u.last_name')
            ->addOrderBy('u.first_name')
            ->addOrderBy('u.patronymic')
            ->groupBy('u.id')
        ;

        $fb = $this->createFormBuilder(null, [
            'csrf_protection'    => false,
            'translation_domain' => false,
        ]);
        $fb->add('legal_entity', EntityType::class, [
            'required'      => false,
            'label'         => 'Юридическое лицо',
            'class'         => 'CommonBundle:LegalEntity',
            'choice_label'  => 'nameAndShortName',
            'placeholder'   => ' - Выберите юридическое лицо - ',
            'mapped'        => false,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('le')
                    ->andWhere('le.active = :active')->setParameter('active', true)
                    ->leftJoin('le.referenceman_operator_envelopes', 'r_env')
                    ->andWhere('r_env.operator IS NOT NULL')
                ;
            },
        ]);
        $fb->add('reference_type', EntityType::class, [
            'label'         => 'Тип бланка',
            'class'         => 'CommonBundle:ReferenceType',
            'placeholder'   => ' - Выберите тип бланка - ',
            'required'      => false,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('rt')
                    ->innerJoin('rt.blanks', 'b', 'WITH', 'b.status = :status')
                    ->setParameter('status', 'acceptedByOperator')
                    ->andWhere('b.referenceman = :referenceman')->setParameter('referenceman', $this->getUser())
                    ;
            },
        ]);
        $fb->add('last_name', TextType::class, [
            'label'    => 'Фамилия',
            'required' => false,
        ]);
        $fb->add('first_name', TextType::class, [
            'label'    => 'Имя',
            'required' => false,
        ]);
        $fb->add('patronymic', TextType::class, [
            'label'    => 'Отчество',
            'required' => false,
        ]);
        $fb->setMethod('get');
        $filterForm = $fb->getForm();

        $filterForm->handleRequest($request);

        $data = $filterForm->get('legal_entity')->getData();
        if ($data) {
            $qb->andWhere('bl.legal_entity = :legal')->setParameter('legal', $data);
        }

        $data = $filterForm->get('reference_type')->getData();
        if ($data) {
            $qb->andWhere('bl.reference_type = :reference_type')->setParameter('reference_type', $data);
        }

        $data = $filterForm->get('last_name')->getData();
        if ($data) {
            $qb->andWhere('u.last_name LIKE :last_name')->setParameter('last_name', '%'.$data.'%');
        }

        $data = $filterForm->get('first_name')->getData();
        if ($data) {
            $qb->andWhere('u.first_name LIKE :first_name')->setParameter('first_name', '%'.$data.'%');
        }

        $data = $filterForm->get('patronymic')->getData();
        if ($data) {
            $qb->andWhere('u.patronymic LIKE :patronymic')->setParameter('patronymic', '%'.$data.'%');
        }

        $operators = $qb->getQuery()->execute();

        $legalsData = $this->em->getRepository('CommonBundle:LegalEntity')->createQueryBuilder('l')
            ->select('l.id AS l_id, l.short_name AS l_short_name')
            ->getQuery()->getArrayResult();

        $legalNames = [];
        foreach ($legalsData as $data) {
            $legalNames[$data['l_id']] = $data['l_short_name'];
        }

        $rTypesData = $this->em->getRepository('CommonBundle:ReferenceType')->createQueryBuilder('rt')
            ->select('rt.id AS rt_id, rt.name AS rt_name')
            ->getQuery()->getArrayResult();

        $typeNames = [];
        foreach ($rTypesData as $data) {
            $typeNames[$data['rt_id']] = $data['rt_name'];
        }

        $opLegalBlanksData = [];
        foreach ($operators as $operator) { /** @var $operator \KreaLab\CommonBundle\Entity\User */
            $qb = $this->em->getRepository('CommonBundle:Blank')->createQueryBuilder('b')
                ->select('DISTINCT (b.legal_entity) AS legal_id, (b.reference_type) AS type_id')
                ->andWhere('b.operator = :operator')->setParameter('operator', $operator)
                ->andWhere('b.status IN (:status)')->setParameter('status', $usedStatus)
                ->andWhere('b.referenceman = :referenceman')->setParameter('referenceman', $this->getUser())
            ;



            $data = $filterForm->get('legal_entity')->getData();
            if ($data) {
                $qb->andWhere('b.legal_entity = :legal')->setParameter('legal', $data);
            }

            $legalAndTypes = $qb->getQuery()->execute();

            if (!$legalAndTypes) {
                continue;
            }

            $ltData = [];
            foreach ($legalAndTypes as $legalAndType) {
                $legal = $legalAndType['legal_id'];
                $type  = $legalAndType['type_id'];

                if (!isset($ltData[$legal]) || !in_array($type, $ltData[$legal])) {
                    $ltData[$legal][] = $type;
                }
            }

            foreach ($ltData as $legalId => $types) {
                foreach ($types as $typeId) {
                    $cntByStatus = $this->em->getRepository('CommonBundle:Blank')->createQueryBuilder('b')
                        ->select('SUM(CASE WHEN b.status = :st1 AND b.status != :st11 THEN 1 ELSE 0 END) AS cnt_accept')
                        ->addSelect('SUM(CASE WHEN b.status IN (:st2) THEN 1 ELSE 0 END) AS cnt_cancelled')
                        ->addSelect('SUM(CASE WHEN b.status = :st3 THEN 1 ELSE 0 END) AS cnt_lost')
                        ->setParameter('st1', 'acceptedByOperator')
                        ->setParameter('st11', 'lost')
                        ->setParameter('st2', ['archivedByReferenceman', 'cancelledByOperator'])
                        ->setParameter('st3', 'lost')
                        ->andWhere('b.legal_entity = :legal')->setParameter('legal', $legalId)
                        ->andWhere('b.status != :status')->setParameter('status', 'usedByOperator')
                        ->andWhere('b.status != :status')->setParameter('status', 'lostChecked')
                        ->andWhere('b.operator = :operator')->setParameter('operator', $operator)
                        ->andWhere('b.referenceman = :referenceman')->setParameter('referenceman', $this->getUser())
                        ->andWhere('b.reference_type = :type')->setParameter('type', $typeId)
                        ->getQuery()->execute();

                    if (count($cntByStatus) == 0) {
                        continue;
                    }

                    $opLegalBlanksData[$operator->getId()][$legalId][$typeId] = $cntByStatus[0];
                }
            }
        }

        return $this->render('AppBundle:Referenceman/ReportingBlanks:list.html.twig', [
            'operators'            => $operators,
            'filter_form'          => $filterForm->createView(),
            'op_legals_blank_data' => $opLegalBlanksData,
            'legal_names'          => $legalNames,
            'type_names'           => $typeNames,
        ]);
    }

    /**
     * @Route("/report-blanks/for-operator-{id}-{leg_id}-{ref_id}-{type}/",
     *     name="rm_report_blanks_list_for_type",
     *     requirements={"id": "\d+", "leg_id": "\d+"})
     */
    public function listForOperatorAction(Request $request)
    {
        $opId      = $request->get('id');
        $legId     = $request->get('leg_id');
        $refTypeId = $request->get('ref_id');
        $type      = $request->get('type');

        if (!$opId || !$legId || !$refTypeId || !$type) {
            throw new NotFoundHttpException('Not found data for find blanks');
        }

        $operator    = $this->em->find('CommonBundle:User', $opId);
        $legalEntity = $this->em->find('CommonBundle:LegalEntity', $legId);
        $refType     = $this->em->find('CommonBundle:ReferenceType', $refTypeId);

        if (!$operator) {
            throw new NotFoundHttpException('Not found operator for id '.$opId);
        }

        if (!$legalEntity) {
            throw new NotFoundHttpException('Not found legal entity for id '.$legId);
        }

        if (!$refType) {
            throw new NotFoundHttpException('Not found reference type for id '.$refTypeId);
        }

        $statuses = [];
        switch ($type) {
            case 'on-hands':
                $statuses = ['acceptedByOperator'];
                break;
            case 'cancelled':
                $statuses = ['archivedByReferenceman', 'cancelledByOperator'];
                break;
            case 'lost':
                $statuses = ['lost'];
                break;
        }

        $qb = $this->em->getRepository('CommonBundle:Blank')->createQueryBuilder('b')
            ->andWhere('b.status IN (:statuses)')->setParameter('statuses', $statuses)
            ->andWhere('b.operator = :operator')->setParameter('operator', $opId)
            ->andWhere('b.referenceman = :referenceman')->setParameter('referenceman', $this->getUser())
            ->andWhere('b.reference_type = :type')->setParameter('type', $refTypeId)
            ->leftJoin('b.legal_entity', 'l')->addSelect('l');

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($request->get('page', 1));

        return $this->render('@App/Referenceman/ReportingBlanks/list_for_operator_by_type.html.twig', [
            'pagerfanta' => $pagerfanta,
            'operator'   => $operator,
            'legal'      => $legalEntity,
            'ref_type'   => $refType,
            'type'       => $type,
        ]);
    }

    /**
     * @Route("/report-blanks/view-logs-{id}/",
     *     name="rm_report_blanks_view_logs_for_blank",
     *     requirements={"id": "\d+"})
     */
    public function viewBlankLogsAction(Request $request)
    {
        $referer = $request->headers->get('referer');
        $blankId = $request->get('id');

        if (!$blankId) {
            throw new NotFoundHttpException('Not found id for find blank');
        }

        /** @var  $blank \KreaLab\CommonBundle\Entity\Blank */
        $blank = $this->em->getRepository('CommonBundle:Blank')->createQueryBuilder('b')
            ->andWhere('b.id = :id')->setParameter('id', $blankId)
            ->leftJoin('b.operator', 'op')->addSelect('op')
            ->leftJoin('b.reference_type', 'rt')->addSelect('rt')
            ->leftJoin('b.legal_entity', 'l')->addSelect('l')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        if (!$blank) {
            throw new NotFoundHttpException('Not found blank');
        }

        $operator = $blank->getOperator();
        $legal    = $blank->getLegalEntity();
        $refType  = $blank->getReferenceType();

        $lifeLogs = $this->em->getRepository('CommonBundle:BlankLifeLog')->createQueryBuilder('ll')
            ->andWhere('ll.blank = :blank_id')->setParameter('blank_id', $blankId)
            ->leftJoin('ll.blank', 'bl')->addSelect('bl')
            ->leftJoin('ll.start_user', 'l_su')->addSelect('l_su')
            ->leftJoin('ll.end_user', 'l_eu')->addSelect('l_eu')
            ->addOrderBy('ll.created_at', 'ASC')
            ->getQuery()->execute();

        $statusesData = [];
        if ($lifeLogs) {
            $trans       = $this->get('translator');
            $transSource = 'reporting_for_reference';

            foreach ($lifeLogs as $lifeLog) {
                /** @var $lifeLog \KreaLab\CommonBundle\Entity\BlankLifeLog */
                $status    = $lifeLog->getOperationStatus();
                $envId     = $lifeLog->getEnvelopeId();
                $workplace = $lifeLog->getWorkplace();

                $startUser    = $lifeLog->getStartUser();
                $endUser      = $lifeLog->getEndUser();
                $transOptions = [
                    '%start_user%'   => $startUser,
                    '%end_user%'     => $endUser,
                    '%env_id%'       => $envId,
                    '%filial%'       => $workplace ? $workplace->getFilial()->getName() : null,
                    '%work_place%'   => $workplace ? $workplace->getName() : null,
                    '%serv_name%'    => $lifeLog->getServiceName(),
                    '%corect_blank%' => $lifeLog->getCorrectBlankNumber(),
                    '%penalty_sum%'  => $lifeLog->getPenaltySum(),
                ];


                $status = $trans->trans($status, $transOptions, $transSource);

                $statusesData[$lifeLog->getId()] = $status;
            }
        }

        return $this->render('@App/Referenceman/ReportingBlanks/view_logs_for_blank.html.twig', [
            'blank'     => $blank,
            'legal'     => $legal,
            'logs'      => $lifeLogs,
            'operator'  => $operator,
            'referer'   => $referer,
            'ref_type'  => $refType,
            'stat_data' => $statusesData,
        ]);
    }
}
